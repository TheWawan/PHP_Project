<?php

namespace App;
/**
* App
*/
class Core
{
	public $name;
	public $url;

	CONST REG_A_HREF	= '/(?<=<a href=")[a-zA-Z0-9- \/.:_áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ]*/';
	CONST REG_IMG_SRC 	= '/(?<=src=")([a-zA-Z0-9-\/ .:_áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ]+)(\.jpg|\.png|\.gif)/';

	public function __construct($name, $url)
	{
		$this->name = $name;
		$this->url 	= $url;
	}

	/**
	 * Get the content of a page and save into a file
	 * @param  string $name Name of the file
	 * @return void
	 */
	public function getSave()
	{
		$page = file_get_contents($this->url);

		file_put_contents('public/' . $this->name , $page);
	}

	/**
	 * Get the content of the selected file
	 * @param  string $name the name of the file
	 * @return string       The content of the file
	 */
	public function getContent()
	{
		return file_get_contents('public/' . $this->name );
	}

	/**
	 * Check if the file exist
	 * @param  string $file fileName
	 * @return bool
	 */
	public function checkFileExist()
	{
		return file_exists('public/' . $this->name);
	}

	/**
	 * Search all a href
	 * @param  string $content The text to search
	 * @return array          All results matchs
	 */
	public function searchA($content)
	{
		preg_match_all(self::REG_A_HREF,$content, $matchs);

		return $matchs;
	}

	/**
	 * Search all img src
	 * @param  string $content The text to search
	 * @return array          All results matchs
	 */
	public function searchSrc($content)
	{
		preg_match_all(self::REG_IMG_SRC,$content, $matchs);

		return $matchs;
	}
}
